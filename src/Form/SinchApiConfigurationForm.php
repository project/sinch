<?php

namespace Drupal\sinch\Form;

use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for Sinch API.
 */
class SinchApiConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sinch_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sinch.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('sinch.api_settings');

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#description' => $this->t('Api token generated from sinch dashboard.'),
      '#default_value' => $config->get('api_token'),
      '#required' => TRUE,
    ];

    $form['service_plan_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Plan ID'),
      '#description' => $this->t('Service Plan ID generated from sinch dashboard.'),
      '#default_value' => $config->get('service_plan_id'),
      '#required' => TRUE,
    ];

    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Sinch phone number'),
      '#description' => $this->t('Phone number from sinch dashboard.'),
      '#required' => TRUE,
      '#default_value' => $config->get('phone_number'),
      '#element_validate' => [
        [
          'Drupal\telephone_validation\Render\Element\TelephoneValidation',
          'validateTel',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sinch.api_settings')
      ->set('api_token', $form_state->getValue('api_token'))
      ->set('service_plan_id', $form_state->getValue('service_plan_id'))
      ->set('phone_number', $form_state->getValue('phone_number'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
