<?php

namespace Drupal\sinch\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sinch\Services\SinchApi;

/**
 * The SinchSmsTestForm provides a simple UI for testing sms sending.
 */
class SinchSmsTestForm extends FormBase {

  /**
   * The sinch api service.
   *
   * @var \Drupal\Sinch\Services\SinchApi
   */
  protected $sinchApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sinch.sinch_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(SinchApi $sinch_api) {
    $this->sinchApi = $sinch_api;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'sinchsmstest';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Recipient phone number'),
      '#required' => TRUE,
      '#element_validate' => [
        [
          'Drupal\telephone_validation\Render\Element\TelephoneValidation',
          'validateTel',
        ],
      ],
    ];

    $form['message'] = [
      '#title' => $this->t('Message'),
      '#type' => 'textarea',
      '#description' => $this->t('Message that will be send'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send sms'),
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $phone_number = $form_state->getValue('phone_number');
    $message = $form_state->getValue('message');
    $this->sinchApi->sendSms($phone_number, $message);
  }
}