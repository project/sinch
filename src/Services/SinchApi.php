<?php

namespace Drupal\sinch\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Component\Serialization\Json;

/**
 * Sinch service provide methods for working with Sinch API.
 */
class SinchApi {

  /**
   * Guzzle Http client.
   *
   * @var \Drupal\sinch\Services\SinchHttpClient
   */
  protected $httpClient;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a YouDataApi object.
   *
   * @param \Drupal\sinch\Services\SinchHttpClient $client
   *   SinchHttp connector.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory instance.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   */
  public function __construct(
    SinchHttpClient $client,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactory $config_factory
  ) {
    $this->httpClient = $client;
    $this->logger = $logger_factory->get('sinch');
    $this->configFactory = $config_factory;
  }

  /**
   * @param $recipient_phone_numbers
   *
   * @return bool|mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendSms($recipient_phone_numbers, $message) {
    $service_plan_id = $this->configFactory->get('sinch.api_settings')->get('service_plan_id');
    $send_from = $this->configFactory->get('sinch.api_settings')->get('phone_number');

    // Check recipient_phone_numbers for multiple numbers and make it an array.
    if (stristr($recipient_phone_numbers, ',')) {
      $recipient_phone_numbers = explode(',', $recipient_phone_numbers);
    }
    else {
      $recipient_phone_numbers = [$recipient_phone_numbers];
    }

    // Set necessary fields to be JSON encoded.
    $content = [
      'to' => array_values($recipient_phone_numbers),
      'from' => $send_from,
      'body' => $message,
    ];

    $data = JSON::encode($content);

    $response = $this->httpClient->request('POST', "/xms/v1/$service_plan_id/batches", [
      'body' => $data,
    ]);

    if ($response->getStatusCode() === 200) {
      return Json::decode($response->getBody()->getContents());
    }
    else {
      $this->logger->error(
        "Assignment is not created." . ' Response code: ' . $response->getStatusCode());

      return FALSE;
    }

  }

}
