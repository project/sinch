<?php

namespace Drupal\sinch\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;

/**
 * Provides prepared http client for working with Sinch Api.
 */
class SinchHttpClient extends Client {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, array $config = []) {
    $this->configFactory = $config_factory;
    $config['base_uri'] = 'https://us.sms.api.sinch.com';

    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  public function request($method, $uri = '', array $options = []) {
    $api_token = $this->configFactory->get('sinch.api_settings')->get('api_token');
    $options['headers']['Authorization'] = "Bearer $api_token";
    $options['headers']['Content-Type'] = 'application/json';

    return parent::request($method, $uri, $options);
  }

}
