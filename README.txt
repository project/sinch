CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The Sinch module provides integration with the Sinch API communication platform
allowing for your Drupal site to integration Voice and SMS functionality.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/sinch


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Visit 'admin/config/system/sinch'.
 * Enter your Sinch account SID, Auth Token, and Phone number found on the Sinch dashboard.
